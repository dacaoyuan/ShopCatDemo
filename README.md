# ShopCatDemo

#### 介绍
仿美团，饿了么购物车加入的效果



#### 使用到的主要框架

1.  implementation 'org.greenrobot:greendao:3.2.0'  (数据库)

2.  implementation 'com.github.CymChad:BaseRecyclerViewAdapterHelper:2.9.30'

3.  implementation 'com.scwang.smartrefresh:SmartRefreshLayout:1.0.5.1'
    implementation 'com.scwang.smartrefresh:SmartRefreshHeader:1.0.5.1'

4.  implementation 'com.flipboard:bottomsheet-core:1.5.3'
    implementation 'com.flipboard:bottomsheet-commons:1.5.3'

5.  implementation 'com.jakewharton:butterknife:8.8.1'
    annotationProcessor 'com.jakewharton:butterknife-compiler:8.8.1'

6.  implementation 'com.google.code.gson:gson:2.2.4'


7.   implementation 'com.oushangfeng:PinnedSectionItemDecoration:1.3.2' (粘性标签，又叫悬浮标题)
     //https://github.com/oubowu/PinnedSectionItemDecoration

#### 使用说明

1. xxxx
2. xxxx
3. xxxx



