package com.example.shopcatdemo.view;

import android.os.CountDownTimer;
import android.widget.TextView;

/**
 * Created by yuanpk on 2018/10/8  11:21
 * <p>
 * Description:
 */
public class CountDownTimerWidget extends CountDownTimer {

    private TextView mTextView; //显示倒计时的文字


    public CountDownTimerWidget(TextView textView) {
        super(60 * 1000, 1 * 1000);
        this.mTextView = textView;

    }

    @Override
    public void onTick(long millisUntilFinished) {
        mTextView.setClickable(false); //设置不可点击
        mTextView.setText(millisUntilFinished / 1000 + "秒"); //设置倒计时时间
    }

    @Override
    public void onFinish() {
        mTextView.setText("重新获取验证码");
        mTextView.setClickable(true);//重新获得点击
    }


}
