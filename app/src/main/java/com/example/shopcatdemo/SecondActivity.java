package com.example.shopcatdemo;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.example.shopcatdemo.adapter.HoverAdapter;
import com.example.shopcatdemo.bean.AllResutl;
import com.example.shopcatdemo.bean.ContentResult;
import com.example.shopcatdemo.bean.LeftResult;
import com.google.gson.Gson;
import com.oushangfeng.pinnedsectionitemdecoration.PinnedHeaderItemDecoration;
import com.oushangfeng.pinnedsectionitemdecoration.callback.OnHeaderClickListener;

import java.util.ArrayList;
import java.util.List;

/**
 * 悬浮标题的功能展示
 * <p>
 * https://github.com/oubowu/PinnedSectionItemDecoration
 */

public class SecondActivity extends AppCompatActivity {
    private static final String TAG = "SecondActivity";
    private RecyclerView mRecyclerView;
    private HoverAdapter mHoverAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scond);

        mRecyclerView = (RecyclerView) findViewById(R.id.recycler_view);

        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mHoverAdapter = new HoverAdapter(null);
        mRecyclerView.setAdapter(mHoverAdapter);
        initData(strJson);

        //https://github.com/oubowu/PinnedSectionItemDecoration
        PinnedHeaderItemDecoration mHeaderItemDecoration = new PinnedHeaderItemDecoration.Builder(ContentResult.TITLE)// 设置粘性标签对应的类型
                .setDividerId(R.color.white) // 设置分隔线资源ID
                .enableDivider(true)   // 开启绘制分隔线，默认关闭
                //.setClickIds(R.id.iv_more, R.id.fl, R.id.checkbox)// 通过传入包括标签和其内部的子控件的ID设置其对应的点击事件
                // .disableHeaderClick(false)  // 是否关闭标签点击事件，默认开启
                .setHeaderClickListener(new OnHeaderClickListener() { // 设置标签和其内部的子控件的监听
                    @Override
                    public void onHeaderClick(View view, int id, int position) {
                        //只有标题在顶部，点击才有效
                        Log.i(TAG, "onHeaderClick: position=" + position);
                    }

                    @Override
                    public void onHeaderLongClick(View view, int id, int position) {
                        //只有标题在顶部，点击才有效
                        Log.i(TAG, "onHeaderLongClick: position=" + position);
                    }
                })
                .create();
        mRecyclerView.addItemDecoration(mHeaderItemDecoration);

        mHoverAdapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
                Log.i(TAG, "onItemClick: position=" + position);
            }
        });
        mHoverAdapter.setOnAnimShopClickListener(new HoverAdapter.OnAnimShopClickListener() {
            @Override
            public void onAddSuccess(View view, int count, int position) {
                Log.i(TAG, "onAddSuccess: count=" + count + " position=" + position);
            }

            @Override
            public void onDelSuccess(View view, int count, int position) {
                Log.i(TAG, "onDelSuccess: count=" + count + " position=" + position);
            }
        });


    }

    private List<LeftResult> mLeftResultList = new ArrayList<>();
    private List<ContentResult> mContentResultList = new ArrayList<>();

    private List<AllResutl.DataBean.ProductBean> productBeanList = new ArrayList<>();

    /**
     * 初始化数据，这里就是造点数据
     */
    private void initData(String strJsonDate) {
        mLeftResultList.clear();
        mContentResultList.clear();
        productBeanList.clear();

        Gson gson = new Gson();
        AllResutl allResutl = gson.fromJson(strJsonDate, AllResutl.class);

        if (allResutl.status == 1) {

            AllResutl.DataBean dataBean = allResutl.data;
            productBeanList = dataBean.product;

            if (productBeanList != null && productBeanList.size() > 0) {

                int i = 0;
                ContentResult contentResult;
                for (AllResutl.DataBean.ProductBean productBean : productBeanList) {
                    String name = productBean.name;


                    contentResult = new ContentResult(ContentResult.TITLE);

                    contentResult.setTitle(name);
                    mContentResultList.add(contentResult);


                    LeftResult leftResult = new LeftResult();
                    leftResult.name = productBean.name;

                    if (i == 0) {
                        leftResult.isSelect = true;
                    } else {
                        leftResult.isSelect = false;
                    }

                    mLeftResultList.add(leftResult);
                    i++;


                    List<AllResutl.DataBean.ProductBean.ListBean> listBean = productBean.list;

                    for (AllResutl.DataBean.ProductBean.ListBean bean : listBean) {
                        contentResult = new ContentResult(ContentResult.CONTENT);
                        contentResult.setProduct_id(bean.product_id);
                        contentResult.setName(bean.name);
                        contentResult.setShopprice(bean.shopprice);
                        contentResult.setInventory(bean.inventory);

                        //contentResult.setCount(0);

                        //这样做，是把左边的item和右边的item做出关联字段,目的是为了解决：
                        //列表滑动过快，firstVisibleItemPosition 没有被赋值，导致左边菜单项，状态设置不对的问题。
                        contentResult.setTitle(name);

                        mContentResultList.add(contentResult);
                    }


                }


                mHoverAdapter.setNewData(mContentResultList);

            }


        } else {
            Log.i(TAG, "initData: allResutl=" + allResutl.msg);
        }

    }


    //模拟json数据
    private String strJson = "{\n" +
            "\t\"msg\": \"请求成功\",\n" +
            "\t\"status\": 1,\n" +
            "\t\"data\": {\n" +
            "\t\t\"product\": [{\n" +
            "\t\t\t\"label_img\": \"http://sq-files.wode369.com/sq-files/other/label/cuxiao.png\",\n" +
            "\t\t\t\"list\": [{\n" +
            "\t\t\t\t\"product_id\": \"3cf3fa3f5de54a4fa5ac98b01a1e850c\",\n" +
            "\t\t\t\t\"inventory\": 5,\n" +
            "\t\t\t\t\"name\": \"苹果\",\n" +
            "\t\t\t\t\"shopprice\": \"1.01\"\n" +
            "\t\t\t}, {\n" +
            "\t\t\t\t\"product_id\": \"8d4528822f1f49889a64985d600d979e\",\n" +
            "\t\t\t\t\"inventory\": 10,\n" +
            "\t\t\t\t\"name\": \"皇家佰瑞特橄榄油（1L)\",\n" +
            "\t\t\t\t\"shopprice\": \"2.01\"\n" +
            "\t\t\t}, {\n" +
            "\t\t\t\t\"product_id\": \"b694faaee23d4fafaa0e8014dd33ad0a\",\n" +
            "\t\t\t\t\"inventory\": 3,\n" +
            "\t\t\t\t\"name\": \"皇家佰瑞特橄榄油（1L)\",\n" +
            "\t\t\t\t\"shopprice\": \"3.01\"\n" +
            "\t\t\t}, {\n" +
            "\t\t\t\t\"product_id\": \"df675b5760724ace879e6f22a2f312a0\",\n" +
            "\t\t\t\t\"inventory\": 0,\n" +
            "\t\t\t\t\"name\": \"皇家佰瑞特橄榄油（1L)\",\n" +
            "\t\t\t\t\"shopprice\": \"4.01\"\n" +
            "\t\t\t}],\n" +
            "\t\t\t\"name\": \"促销\"\n" +
            "\t\t}, {\n" +
            "\t\t\t\"label_img\": \"http://sq-files.wode369.com/sq-files/other/label/cuxiao.png\",\n" +
            "\t\t\t\"list\": [{\n" +
            "\t\t\t\t\"product_id\": \"3cf3fa3f5de54a4fa5ac98b01a1e850c\",\n" +
            "\t\t\t\t\"inventory\": 45,\n" +
            "\t\t\t\t\"name\": \"苹果\",\n" +
            "\t\t\t\t\"shopprice\": \"1.01\"\n" +
            "\t\t\t}, {\n" +
            "\t\t\t\t\"product_id\": \"4ec52417d8504749ac605408834c6366\",\n" +
            "\t\t\t\t\"inventory\": 45,\n" +
            "\t\t\t\t\"name\": \"皇家佰瑞特橄榄油（1L)\",\n" +
            "\t\t\t\t\"shopprice\": \"0.01\"\n" +
            "\t\t\t}, {\n" +
            "\t\t\t\t\"product_id\": \"4ec52417d8504749ac605408834c6377\",\n" +
            "\t\t\t\t\"inventory\": 45,\n" +
            "\t\t\t\t\"name\": \"皇家佰瑞特橄榄油（1L)\",\n" +
            "\t\t\t\t\"shopprice\": \"0.01\"\n" +
            "\t\t\t}, {\n" +
            "\t\t\t\t\"product_id\": \"4ec52417d8504749ac605408834c6388\",\n" +
            "\t\t\t\t\"inventory\": 45,\n" +
            "\t\t\t\t\"name\": \"皇家佰瑞特橄榄油（1L)\",\n" +
            "\t\t\t\t\"shopprice\": \"0.01\"\n" +
            "\t\t\t}],\n" +
            "\t\t\t\"name\": \"优选\"\n" +
            "\t\t}, {\n" +
            "\t\t\t\"label_img\": \"http://sq-files.wode369.com/sq-files/other/label/cuxiao.png\",\n" +
            "\t\t\t\"list\": [{\n" +
            "\t\t\t\t\"product_id\": \"4ec52417d8504749ac605408834c6001\",\n" +
            "\t\t\t\t\"inventory\": 45,\n" +
            "\t\t\t\t\"name\": \"皇家佰瑞特橄榄油（1L)\",\n" +
            "\t\t\t\t\"shopprice\": \"0.01\"\n" +
            "\t\t\t}, {\n" +
            "\t\t\t\t\"product_id\": \"4ec52417d8504749ac605408834c6002\",\n" +
            "\t\t\t\t\"inventory\": 45,\n" +
            "\t\t\t\t\"name\": \"皇家佰瑞特橄榄油（1L)\",\n" +
            "\t\t\t\t\"shopprice\": \"0.01\"\n" +
            "\t\t\t}, {\n" +
            "\t\t\t\t\"product_id\": \"4ec52417d8504749ac605408834c6003\",\n" +
            "\t\t\t\t\"inventory\": 45,\n" +
            "\t\t\t\t\"name\": \"皇家佰瑞特橄榄油（1L)\",\n" +
            "\t\t\t\t\"shopprice\": \"0.01\"\n" +
            "\t\t\t}, {\n" +
            "\t\t\t\t\"product_id\": \"4ec52417d8504749ac605408834c6004\",\n" +
            "\t\t\t\t\"inventory\": 45,\n" +
            "\t\t\t\t\"name\": \"皇家佰瑞特橄榄油（1L)\",\n" +
            "\t\t\t\t\"shopprice\": \"0.01\"\n" +
            "\t\t\t}, {\n" +
            "\t\t\t\t\"product_id\": \"4ec52417d8504749ac605408834c6005\",\n" +
            "\t\t\t\t\"inventory\": 45,\n" +
            "\t\t\t\t\"name\": \"皇家佰瑞特橄榄油（1L)\",\n" +
            "\t\t\t\t\"shopprice\": \"0.01\"\n" +
            "\t\t\t}, {\n" +
            "\t\t\t\t\"product_id\": \"4ec52417d8504749ac605408834c6006\",\n" +
            "\t\t\t\t\"inventory\": 45,\n" +
            "\t\t\t\t\"name\": \"皇家佰瑞特橄榄油（1L)\",\n" +
            "\t\t\t\t\"shopprice\": \"0.01\"\n" +
            "\t\t\t}, {\n" +
            "\t\t\t\t\"product_id\": \"4ec52417d8504749ac605408834c6007\",\n" +
            "\t\t\t\t\"inventory\": 45,\n" +
            "\t\t\t\t\"name\": \"皇家佰瑞特橄榄油（1L)\",\n" +
            "\t\t\t\t\"shopprice\": \"0.01\"\n" +
            "\t\t\t}],\n" +
            "\t\t\t\"name\": \"蔬菜\"\n" +
            "\t\t}]\n" +
            "\t}\n" +
            "}";


}
