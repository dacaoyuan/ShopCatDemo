package com.example.shopcatdemo;

import android.app.Application;

import com.example.shopcatdemo.sql.dao.DaoSession;
import com.example.shopcatdemo.utils.SqlUtils;

public class InitApplication extends Application {


    private static DaoSession daoSession;

    public static DaoSession getDaoInstant() {
        return daoSession;
    }


    @Override
    public void onCreate() {
        super.onCreate();

        daoSession = SqlUtils.setupDatabase(this);  //配置数据库
    }
}
