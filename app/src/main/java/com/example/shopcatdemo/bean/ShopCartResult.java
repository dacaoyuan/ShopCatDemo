package com.example.shopcatdemo.bean;

public class ShopCartResult {
    private int mCount;
    private int mMaxCount;

    private String name;
    private String price;

    private String product_id;

    private String ids;

    public String getIds() {
        return ids;
    }

    public void setIds(String ids) {
        this.ids = ids;
    }

    public String getProduct_id() {
        return product_id;
    }

    public void setProduct_id(String product_id) {
        this.product_id = product_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public ShopCartResult(int maxCount, int count) {
        mCount = count;
        mMaxCount = maxCount;
    }


    public void setCount(int count) {
        mCount = count;
    }

    public int getCount() {
        return mCount;
    }

    public int getMaxCount() {
        return mMaxCount;
    }

    public void setMaxCount(int maxCount) {
        mMaxCount = maxCount;
    }
}
