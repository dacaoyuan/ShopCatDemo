package com.example.shopcatdemo.bean;

import com.chad.library.adapter.base.entity.MultiItemEntity;

/**
 * 作者：ypk
 * 时间：2019/1/9 0009 16:45
 * 描述：
 */

public class ContentResult implements MultiItemEntity {

    public static final int TITLE = 1;
    public static final int CONTENT = 2;
    private int itemType;

    public ContentResult(int itemType) {
        this.itemType = itemType;
    }

    @Override
    public int getItemType() {
        return itemType;
    }

    private String product_id;

    private String title;

    private String name;

    private int inventory;

    private int mCount;

    private String shopprice;

    public String getProduct_id() {
        return product_id;
    }

    public void setProduct_id(String product_id) {
        this.product_id = product_id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getInventory() {
        return inventory;
    }

    public void setInventory(int inventory) {
        this.inventory = inventory;
    }

    public int getCount() {
        return mCount;
    }

    public void setCount(int count) {
        mCount = count;
    }

    public String getShopprice() {
        return shopprice;
    }

    public void setShopprice(String shopprice) {
        this.shopprice = shopprice;
    }
}
