package com.example.shopcatdemo.bean;

import java.util.List;

/**
 * 作者：ypk
 * 时间：2019/1/9 0009 17:21
 * 描述：
 */

public class AllResutl {

    /**
     * data : {"product":[{"label_img":"http://nl-files.wode369.com/sd ","list":[{"add_time":"","channelsite_id":"","description":"描述","grade":5,"ids":"","image":"sd ","inventory":19,"is_follow":0,"is_having":0,"is_upload":0,"label":"111","label_name":"促销","marketprice":"68","name":"蓝溪谷特色跑山鸡蛋（8枚）","send_time":"2018-10-15 16:37:37","shopprice":"40","show_img":"files/upload/d7620c55c838433f976ee46ac94f673c.jpg","showimg":"http://nl-files.wode369.com/files/upload/9044b00e559440e1b1191f4f09f770be.jpg","site_id":"8cc3c64958e343b5a30c7038fe79d40a","sort":1,"title":"蓝溪谷","number":"1","is_best":1,"is_sale":1,"user_id":""}],"name":"促销"}]}
     * msg :
     * status : 1
     */

    public DataBean data;
    public String msg;
    public int status;


    public static class DataBean {
        public List<ProductBean> product;

        public static class ProductBean {
            /**
             * label_img : http://nl-files.wode369.com/sd
             * list : [{"add_time":"","channelsite_id":"","description":"描述","grade":5,"ids":"","image":"sd ","inventory":19,"is_follow":0,"is_having":0,"is_upload":0,"label":"111","label_name":"促销","marketprice":"68","name":"蓝溪谷特色跑山鸡蛋（8枚）","send_time":"2018-10-15 16:37:37","shopprice":"40","show_img":"files/upload/d7620c55c838433f976ee46ac94f673c.jpg","showimg":"http://nl-files.wode369.com/files/upload/9044b00e559440e1b1191f4f09f770be.jpg","site_id":"8cc3c64958e343b5a30c7038fe79d40a","sort":1,"title":"蓝溪谷","number":"1","is_best":1,"is_sale":1,"user_id":""}]
             * name : 促销
             */

            public String label_img;
            public String name;
            public List<ListBean> list;


            public static class ListBean {
                public int inventory;

                public String name;

                public String shopprice;

                public String product_id;

            }
        }
    }
}
