package com.example.shopcatdemo.sql.bean;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Unique;

/**
 * Created by yuanpk on 2018/10/24  17:36
 * <p>
 * Description:TODO
 */
@Entity
public class ShopCartBean {

    //不能用int (autoincrement = true)表示主键会自增，如果false就会使用旧值
    @Id(autoincrement = true)
    private Long id;

    @Unique
    private String ids;//这个是数据库中的主键

    private String user_id;

    private String product_id;

    private String product_name;

    private int inventory;//这个是购物车库存

    private String price; //单价

    private int quantity;//这个是购物车商品的数量，而不是库存

    @Generated(hash = 876675747)
    public ShopCartBean(Long id, String ids, String user_id, String product_id,
            String product_name, int inventory, String price, int quantity) {
        this.id = id;
        this.ids = ids;
        this.user_id = user_id;
        this.product_id = product_id;
        this.product_name = product_name;
        this.inventory = inventory;
        this.price = price;
        this.quantity = quantity;
    }

    @Generated(hash = 656791369)
    public ShopCartBean() {
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getIds() {
        return this.ids;
    }

    public void setIds(String ids) {
        this.ids = ids;
    }

    public String getUser_id() {
        return this.user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getProduct_id() {
        return this.product_id;
    }

    public void setProduct_id(String product_id) {
        this.product_id = product_id;
    }

    public String getProduct_name() {
        return this.product_name;
    }

    public void setProduct_name(String product_name) {
        this.product_name = product_name;
    }

    public int getInventory() {
        return this.inventory;
    }

    public void setInventory(int inventory) {
        this.inventory = inventory;
    }

    public String getPrice() {
        return this.price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public int getQuantity() {
        return this.quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }


}
