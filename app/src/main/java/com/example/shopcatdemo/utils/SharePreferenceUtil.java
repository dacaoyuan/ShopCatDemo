package com.example.shopcatdemo.utils;

import android.content.Context;
import android.content.SharedPreferences;

public class SharePreferenceUtil {
    private SharedPreferences mSharedPreferences;
    private static SharedPreferences.Editor editor;
    private static SharePreferenceUtil mPreferenceUtils;

    public SharePreferenceUtil(Context context) {
        mSharedPreferences = context.getSharedPreferences("demo", Context.MODE_PRIVATE);
        editor = mSharedPreferences.edit();
    }


    public static synchronized SharePreferenceUtil getInstance(Context cxt) {
        if (mPreferenceUtils == null) {
            mPreferenceUtils = new SharePreferenceUtil(cxt);
        }

        return mPreferenceUtils;
    }


    public String getUserId() {
        return mSharedPreferences.getString("userId", "");
    }

    public void setUserId(String href) {
        editor.putString("userId", href);
        editor.commit();
    }


}
