package com.example.shopcatdemo.utils;

import java.math.BigDecimal;
import java.util.Calendar;

/**
 * Created by yuanpk on 2018/11/9  11:37
 * <p>
 * Description:金额计算工具类
 */
public class AmountCalculationUtils {


    /**
     * 加
     *
     * @param strAmount1
     * @param strAmount2
     */
    public static String add(String strAmount1, String strAmount2) {
        BigDecimal a = new BigDecimal(strAmount1);
        BigDecimal b = new BigDecimal(strAmount2);
        BigDecimal bigDecimal = a.add(b);
        return bigDecimal.toString();

    }

    public static Float addF(String strAmount1, String strAmount2) {
        BigDecimal a = new BigDecimal(strAmount1);
        BigDecimal b = new BigDecimal(strAmount2);
        BigDecimal bigDecimal = a.add(b);
        return bigDecimal.floatValue();

    }

    /**
     * 减
     *
     * @param strAmount1
     * @param strAmount2
     */
    public static String subtract(String strAmount1, String strAmount2) {
        BigDecimal a = new BigDecimal(strAmount1);
        BigDecimal b = new BigDecimal(strAmount2);
        BigDecimal bigDecimal = a.subtract(b);
        return bigDecimal.toString();
    }

    /**
     * 乘
     *
     * @param strAmount1
     * @param strAmount2
     */
    public static String multiplyS(String strAmount1, String strAmount2) {
        BigDecimal a = new BigDecimal(strAmount1);
        BigDecimal b = new BigDecimal(strAmount2);
        BigDecimal bigDecimal = a.multiply(b);

        return bigDecimal.toString();
    }

    /**
     * 乘
     *
     * @param strAmount1
     * @param strAmount2
     */
    public static float multiplyF(String strAmount1, String strAmount2) {
        BigDecimal a = new BigDecimal(strAmount1);
        BigDecimal b = new BigDecimal(strAmount2);
        BigDecimal bigDecimal = a.multiply(b);
        return bigDecimal.floatValue();
    }


    /**
     * 除
     *
     * @param strAmount1
     * @param strAmount2
     */
    public static String divide(String strAmount1, String strAmount2) {
        BigDecimal a = new BigDecimal(strAmount1);
        BigDecimal b = new BigDecimal(strAmount2);
        BigDecimal bigDecimal = a.divide(b);
        return bigDecimal.toString();

    }

    /**
     * 设置精度,取两位数
     *
     * @param strAmount
     */
    public static String setPrecision(String strAmount) {
        BigDecimal amountBigDecimal = new BigDecimal(strAmount);
        BigDecimal bigDecimal = amountBigDecimal.setScale(2, BigDecimal.ROUND_HALF_UP);
        return bigDecimal.toString();
    }

    /**
     * 取反(其实就是负值)
     *
     * @param strAmount
     */
    public static String negate(String strAmount) {
        BigDecimal amountBigDecimal = new BigDecimal(strAmount);
        BigDecimal bigDecimal = amountBigDecimal.negate();
        return bigDecimal.toString();
    }




    /*public static void main(String args[]) {

        //String multiply = multiply("0.001", "5");
        //String s = negate(multiply);

        String s = divide("6", "4");
        System.out.println("AmountCalculationUtils.main aa=" + s);
    }*/

    /*public static void main(String args[]) {
        System.out.println("AmountCalculationUtils.main=" + 2 * 60 * 60 * 1000);
        int time = (57143) / 1000;
        int hou = 0, mi = 0, s = 0;//3587143
        hou = time / (60 * 60);
        if (hou == 0) {
            mi = time / 60;
            if (mi == 0) {
                s = time;
            } else {
                s = time % 60;
            }
        } else {
            mi = time % (60 * 60) / 60;
            s = time % (60 * 60) % 60;
        }
        System.out.println("AmountCalculationUtils.main hour=" + hou + " min=" + mi + " second=" + s);

    }*/

    /*public static void main(String args[]) {
        for (int i = 0; i < 10; i++) {
            if (i == 7) {
                // continue;
                break;
            }
            System.out.println("AmountCalculationUtils.main i=" + i);
        }

        System.out.println("AmountCalculationUtils.main1111");

    }*/


}
