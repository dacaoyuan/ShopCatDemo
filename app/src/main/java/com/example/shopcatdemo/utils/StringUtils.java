package com.example.shopcatdemo.utils;

import java.util.UUID;

public class StringUtils {

    /**
     * @return 返回32位uuid, 作为数据库中的主键
     */
    public static String getUUID() {
        return UUID.randomUUID().toString().replace("-", "");
    }
}
