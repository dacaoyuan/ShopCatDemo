package com.example.shopcatdemo.utils;

import android.app.Application;
import android.database.sqlite.SQLiteDatabase;

import com.example.shopcatdemo.sql.dao.DaoMaster;
import com.example.shopcatdemo.sql.dao.DaoSession;


/**
 * Created by yuanpk on 2018/10/25  9:03
 * <p>
 * Description:TODO
 */
public class SqlUtils {

    /**
     * 配置数据库
     */
    public static DaoSession setupDatabase(Application application) {
        //创建数据库shop.db"
        DaoMaster.DevOpenHelper helper = new DaoMaster.DevOpenHelper(application, "demo.db", null);
        //获取可写数据库
        SQLiteDatabase db = helper.getWritableDatabase();
        //获取数据库对象
        DaoMaster daoMaster = new DaoMaster(db);
        //获取Dao对象管理者
        return daoMaster.newSession();

    }


    //根据唯一约束字段 Product_id ，查询该行数据
        /* ShopCartBean cartBean = cartBeanDao.queryBuilder()
                        .where(ShopCartBeanDao.Properties.Product_id.eq(itemData.getIds()))
                        .build()
                        .unique();
                shopCartBean.setId(cartBean.getId());
                cartBeanDao.delete(shopCartBean);*/

    //根据字段 Product_id ，查询Product_id=指定数据 的所有数据，是个list
        /*  List<ShopCartBean> cartBeanList = cartBeanDao.queryBuilder()
                        .where(ShopCartBeanDao.Properties.Product_id.eq(itemData.getIds()))
                        .build()
                        .list();*/

    // cartBeanDao.insertOrReplace(shopCartBean);



}
