package com.example.shopcatdemo.adapter;

import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.widget.LinearLayout;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.example.shopcatdemo.R;
import com.example.shopcatdemo.bean.LeftResult;

import java.util.List;

/**
 * 作者：ypk
 * 时间：2019/1/9 0009 16:24
 * 描述：
 */

public class LeftAdapter extends BaseQuickAdapter<LeftResult, BaseViewHolder> {

    public LeftAdapter(@Nullable List<LeftResult> data) {
        super(R.layout.item_left, data);
    }

    @Override
    protected void convert(BaseViewHolder helper, LeftResult item) {
        helper.setText(R.id.text, item.name);
        LinearLayout helperView = helper.getView(R.id.item_layout);
        if (item.isSelect) {
            helperView.setBackground(ContextCompat.getDrawable(mContext, R.drawable.text_pressed));
        } else {
            helperView.setBackground(ContextCompat.getDrawable(mContext, R.color.page_bg));
        }

    }
}
