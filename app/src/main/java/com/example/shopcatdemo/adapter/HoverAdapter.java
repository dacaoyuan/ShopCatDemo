package com.example.shopcatdemo.adapter;

import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.RotateAnimation;
import android.view.animation.TranslateAnimation;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.chad.library.adapter.base.BaseMultiItemQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.example.shopcatdemo.R;
import com.example.shopcatdemo.bean.ContentResult;
import com.oushangfeng.pinnedsectionitemdecoration.utils.FullSpanUtil;

import java.util.List;

/**
 * 悬停标题，adapter 用户展示,以及添加商品和减少商品的动画处理
 * 直接继承BaseMultiItemQuickAdapter单独实现一个适配器的写法
 */
public class HoverAdapter extends BaseMultiItemQuickAdapter<ContentResult, BaseViewHolder> {

    private int number = 0;

    public HoverAdapter(List<ContentResult> data) {
        super(data);
        addItemType(ContentResult.TITLE, R.layout.item_title);
        addItemType(ContentResult.CONTENT, R.layout.item_hover);
    }

    //如果只是垂直列表排列，可不用添加这些代码
   /* @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
        //Adapter记得要实现对网格布局和瀑布流布局的标签占满一行的处理，调用FullSpanUtil工具类进行处理

        FullSpanUtil.onAttachedToRecyclerView(recyclerView, this, ContentResult.TITLE);
    }*/

    //如果只是垂直列表排列，可不用添加这些代码
   /* @Override
    public void onViewAttachedToWindow(BaseViewHolder holder) {
        super.onViewAttachedToWindow(holder);
        //Adapter记得要实现对网格布局和瀑布流布局的标签占满一行的处理，调用FullSpanUtil工具类进行处理
        FullSpanUtil.onViewAttachedToWindow(holder, this, ContentResult.CONTENT);
    }*/

    @Override
    protected void convert(BaseViewHolder helper, ContentResult item) {
        switch (helper.getItemViewType()) {

            case ContentResult.TITLE:
                helper.setText(R.id.tv_title, item.getTitle());
                break;

            case ContentResult.CONTENT:


               helper.setText(R.id.name, item.getName());
                helper.setText(R.id.tv_price, item.getShopprice());

                TextView tvNumber = helper.getView(R.id.tv_number);

                ImageView imageRemove = helper.getView(R.id.iv_remove);
                ImageView imageAdd = helper.getView(R.id.iv_add);

                if (item.getCount() > 0) {
                    tvNumber.setVisibility(View.VISIBLE);
                    imageRemove.setVisibility(View.VISIBLE);
                    tvNumber.setText(item.getCount() + "");
                } else {
                    tvNumber.setVisibility(View.INVISIBLE);
                    imageRemove.setVisibility(View.INVISIBLE);
                    tvNumber.setText("0");

                }

                if (item.getInventory() <= 0) {
                    tvNumber.setVisibility(View.INVISIBLE);
                    imageRemove.setVisibility(View.INVISIBLE);
                }


                //减少
                imageRemove.setOnClickListener(v -> {
                    number = item.getCount();
                    if (number == 0) return;

                    number--;
                    item.setCount(number);
                    tvNumber.setText(number + "");

                    if (number == 0) {
                        tvNumber.setVisibility(View.INVISIBLE);
                        imageRemove.setVisibility(View.INVISIBLE);
                        tvNumber.setAnimation(getHiddenAnimation());
                        imageRemove.setAnimation(getHiddenAnimation());
                    }

                    listener.onDelSuccess(imageAdd, number, helper.getLayoutPosition());
                });

                //添加
                imageAdd.setOnClickListener(v -> {
                    number = item.getCount();

                    if (number < item.getInventory()) {
                        number++;
                        item.setCount(number);
                        tvNumber.setText(number + "");

                        if (number == 1) {
                            tvNumber.setVisibility(View.VISIBLE);
                            imageRemove.setVisibility(View.VISIBLE);
                            tvNumber.setAnimation(getShowAnimation());
                            imageRemove.setAnimation(getShowAnimation());
                        }

                        listener.onAddSuccess(imageAdd, number, helper.getLayoutPosition());
                    } else {
                        Toast.makeText(mContext, "已超出库存数量", Toast.LENGTH_SHORT).show();
                    }

                });


                break;

        }
    }

    public interface OnAnimShopClickListener {
        void onAddSuccess(View view, int count, int position);

        void onDelSuccess(View view, int count, int position);
    }

    public OnAnimShopClickListener listener;

    public void setOnAnimShopClickListener(OnAnimShopClickListener listener) {
        if (listener != null) {
            this.listener = listener;
        }
    }


    //显示减号的动画
    private Animation getShowAnimation() {
        AnimationSet set = new AnimationSet(true);
        RotateAnimation rotate = new RotateAnimation(0, 720, RotateAnimation.RELATIVE_TO_SELF, 0.5f, RotateAnimation.RELATIVE_TO_SELF, 0.5f);
        set.addAnimation(rotate);
        TranslateAnimation translate = new TranslateAnimation(
                TranslateAnimation.RELATIVE_TO_SELF, 2f
                , TranslateAnimation.RELATIVE_TO_SELF, 0
                , TranslateAnimation.RELATIVE_TO_SELF, 0
                , TranslateAnimation.RELATIVE_TO_SELF, 0);
        set.addAnimation(translate);
        AlphaAnimation alpha = new AlphaAnimation(0, 1);
        set.addAnimation(alpha);
        set.setDuration(300);
        return set;
    }

    //隐藏减号的动画
    private Animation getHiddenAnimation() {
        AnimationSet set = new AnimationSet(true);
        RotateAnimation rotate = new RotateAnimation(0, 720, RotateAnimation.RELATIVE_TO_SELF, 0.5f, RotateAnimation.RELATIVE_TO_SELF, 0.5f);
        set.addAnimation(rotate);
        TranslateAnimation translate = new TranslateAnimation(
                TranslateAnimation.RELATIVE_TO_SELF, 0
                , TranslateAnimation.RELATIVE_TO_SELF, 2f
                , TranslateAnimation.RELATIVE_TO_SELF, 0
                , TranslateAnimation.RELATIVE_TO_SELF, 0);
        set.addAnimation(translate);
        AlphaAnimation alpha = new AlphaAnimation(1, 0);
        set.addAnimation(alpha);
        set.setDuration(300);
        return set;
    }


}
