package com.example.shopcatdemo.adapter;

import android.support.annotation.Nullable;
import android.view.View;

import com.chad.library.adapter.base.BaseMultiItemQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.example.shopcatdemo.R;
import com.example.shopcatdemo.bean.ContentResult;
import com.example.shopcatdemo.view.shopcart.AnimShopButton;
import com.example.shopcatdemo.view.shopcart.IOnAddDelListener;

import java.util.List;

/**
 * 作者：ypk
 * 时间：2019/1/9 0009 16:44
 * 描述：
 */

public class ContentAdapter extends BaseMultiItemQuickAdapter<ContentResult, BaseViewHolder> {

    public ContentAdapter(@Nullable List<ContentResult> data) {
        super(data);
        addItemType(ContentResult.TITLE, R.layout.item_title);
        addItemType(ContentResult.CONTENT, R.layout.item_content);
    }


    @Override
    protected void convert(final BaseViewHolder helper, final ContentResult item) {

        if (item.getItemType() == ContentResult.TITLE) {

            helper.setText(R.id.tv_title, item.getTitle());

        } else if (item.getItemType() == ContentResult.CONTENT) {

            helper.setText(R.id.name, item.getName());
            helper.setText(R.id.tv_price, item.getShopprice());

            helper.addOnClickListener(R.id.btn_add_cart);

            final AnimShopButton addCartButton = helper.getView(R.id.btn_add_cart);
            addCartButton.setMaxCount(item.getInventory());

            //在item复用的时候，把添加的个数恢复
            addCartButton.setCount(item.getCount());

            addCartButton.setOnAddDelListener(new IOnAddDelListener() {
                @Override
                public void onAddSuccess(int count) {
                    item.setCount(count);
                    listener.onAddSuccess(addCartButton, count, helper.getLayoutPosition());
                }

                @Override
                public void onAddFailed(int count, FailType failType) {

                }

                @Override
                public void onDelSuccess(int count) {
                    item.setCount(count);
                    listener.onDelSuccess(addCartButton, count, helper.getLayoutPosition());
                }

                @Override
                public void onDelFailure(int count, FailType failType) {

                }
            });

        }

    }


    public interface OnAnimShopClickListener {
        void onAddSuccess(View view, int count, int position);

        //void onAddFailed(int count, IOnAddDelListener.FailType failType);
        void onDelSuccess(View view, int count, int position);
        //void onDelFailure(int count, IOnAddDelListener.FailType failType);
    }

    public OnAnimShopClickListener listener;

    public void setOnAnimShopClickListener(OnAnimShopClickListener listener) {
        this.listener = listener;
    }
}