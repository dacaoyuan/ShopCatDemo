package com.example.shopcatdemo.adapter;

import android.support.annotation.Nullable;
import android.view.View;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.example.shopcatdemo.R;
import com.example.shopcatdemo.bean.ShopCartResult;
import com.example.shopcatdemo.view.shopcart.AnimShopButton;
import com.example.shopcatdemo.view.shopcart.IOnAddDelListener;

import java.util.List;

/**
 * Created by yuanpk on 2018/10/18  17:17
 * <p>
 * Description:TODO //
 */
public class ShopCartAdapter extends BaseQuickAdapter<ShopCartResult, BaseViewHolder> {


    public ShopCartAdapter(@Nullable List<ShopCartResult> data) {
        super(R.layout.item_shop_cart, data);
    }

    @Override
    protected void convert(final BaseViewHolder helper, final ShopCartResult item) {


        helper.setText(R.id.goodsName, item.getName());
        helper.setText(R.id.price, "单价：¥ " + item.getPrice());


        final AnimShopButton addCartButton = helper.getView(R.id.shopCartButton);
        addCartButton.setCount(item.getCount());

        addCartButton.setMaxCount(item.getMaxCount());


        addCartButton.setOnAddDelListener(new IOnAddDelListener() {
            @Override
            public void onAddSuccess(int count) {
                item.setCount(count);
                listener.onAddSuccess(addCartButton, count, helper.getLayoutPosition());
            }

            @Override
            public void onAddFailed(int count, FailType failType) {

            }

            @Override
            public void onDelSuccess(int count) {
                item.setCount(count);
                listener.onDelSuccess(addCartButton, count, helper.getLayoutPosition());
            }

            @Override
            public void onDelFailure(int count, FailType failType) {

            }
        });

    }

    public interface OnAnimShopClickListener {
        void onAddSuccess(View view, int count, int position);

        //void onAddFailed(int count, IOnAddDelListener.FailType failType);
        void onDelSuccess(View view, int count, int position);
        //void onDelFailure(int count, IOnAddDelListener.FailType failType);
    }

    public OnAnimShopClickListener listener;

    public void setOnAnimShopClickListener(OnAnimShopClickListener listener) {
        this.listener = listener;
    }
}
